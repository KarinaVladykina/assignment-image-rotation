const char* const write_status_out[] = { 
 [WRITE_OK] = "Данные записаны успешно\n", 
 [WRITE_ERROR] = "Данные не были записаны\n", 
 [WRITE_FILE_ERROR] = "Данные в файл не записаны\n", 
 [WRITE_FILE_SUCCESS] = "Данные успешно записаны в файл\n",
}; 
 
const char* const read_status_out[] = { 
 [READ_OK] = "Данные успешно считаны\n", 
 [READ_INVALID_HEADER] = "Не удалось прочитать заголовок\n", 
 [READ_FILE_SUCCESS] = "Файл успешно считан\n", 
 [READ_FILE_ERROR] = "Не удалось прочитать файл\n", 
}; 
