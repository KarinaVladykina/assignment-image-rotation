#ifndef FILE_OPS 
#define FILE_OPS 
#include <stdio.h> 
#include "bmp.h" 
#include "image.h" 
#include "status.h" 
 
enum file_status { 
 CLOSE_FILE_OK, 
 CLOSE_FILE_ERROR 
}; 
 
 
enum read_status get_image(char* const filename, struct image* const image); 
enum write_status write_image(char* const filename, struct image* const image); 
 
 
#endif
