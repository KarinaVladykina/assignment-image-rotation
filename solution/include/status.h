#ifndef STATUS
#define STATUS

enum  write_status {
	WRITE_OK = 0,
	WRITE_FILE_SUCCESS = 1,
	WRITE_ERROR = 2,
	WRITE_FILE_ERROR = 3,
	ERROR_CLOSE = 4
};

enum read_status {
	READ_OK = 0,
	READ_FILE_SUCCESS = 1,
	READ_INVALID_HEADER = 2,
	READ_FILE_ERROR = 3,
	FILE_ERR_CLOSE = 4
};
#endif


