#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "image.h"
#include "status.h"


#ifndef BMP
#define BMP

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

#endif

