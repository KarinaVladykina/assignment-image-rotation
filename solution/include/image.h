#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef IMAGE_STRUCTS
#define IMAGE_STRUCTS
struct __attribute__((__packed__))pixel { uint8_t b, g, r; };

struct image {
	uint64_t width, height;
	struct pixel* data;
};
#endif

struct pixel* amount_of_pixel(uint32_t x, uint32_t y );

struct image create_img(size_t width, size_t height);

void delete_image(struct image img);
