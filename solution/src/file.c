#include "file.h" 
 
enum read_status get_image(char* const filename, struct image* const image) { 
 FILE* filePointer; 
 filePointer = fopen(filename, "r"); 
 if (filePointer == NULL) 
  return READ_FILE_ERROR; 
 enum read_status read_bmp_status = from_bmp(filePointer, image); 
 if (read_bmp_status != READ_OK) 
  return read_bmp_status; 
 if (fclose(filePointer) == EOF){ 
  fprintf(stderr,"%s", "Error when closing the input file!"); 
    exit(1); 
 
 } 
 return READ_FILE_SUCCESS; 
} 
 
enum write_status write_image(char* const filename, struct image* const image) { 
 FILE* filePointer; 
 filePointer = fopen(filename, "w"); 
 if (filePointer == NULL) 
  return WRITE_FILE_ERROR; 
 enum write_status write_bmp_status = to_bmp(filePointer, image); 
 if (write_bmp_status != WRITE_OK) 
  return write_bmp_status; 
 if (fclose(filePointer) == EOF){ 
   fprintf(stderr,"%s", "Error when closing the output file!"); 
    exit(1); 
 } 
 return WRITE_FILE_SUCCESS; 
}
