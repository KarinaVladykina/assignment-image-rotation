#include "bmp.h"
#pragma pack(push, 1)

#define TYPE 19778
#define PLANES 1
#define DIB_SIZE 40
#define BPP 24
#define COMP 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0
#define HEADER_SIZE 54


struct __attribute__((__packed__))bmp_header {
    uint16_t bfType;
    uint32_t bFileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)



static enum read_status read_header(FILE* in, struct bmp_header* header) {

	if (fread(header, sizeof(struct bmp_header), 1, in) == 1)
		return READ_OK;
	else return READ_INVALID_HEADER;
}

static uint32_t define_padding(uint32_t width) {
	uint32_t padding;
	if (width % 4 == 0) {
		padding = 0;
	}
	else padding = 4 * ((width * 3 / 4) + 1) - 3 * width;
	return padding;
}

static enum write_status write_header(FILE* out, struct image const* img) {
	uint32_t const padding = define_padding(img->width);
	struct bmp_header header = { 0 };
	size_t image_size = sizeof(struct pixel) * img->width * img->height + padding * img->height;
	header.bfType = TYPE;
	header.bFileSize = HEADER_SIZE + image_size;
	header.bOffBits = HEADER_SIZE;
	header.biSize = DIB_SIZE;
	header.biWidth = img->width;
	header.biHeight = img->height;
	header.biPlanes = PLANES;
	header.biBitCount = BPP;
	header.biCompression = COMP;
	header.biSizeImage = image_size;
	header.biXPelsPerMeter = X_PPM;
	header.biYPelsPerMeter = Y_PPM;
	header.biClrUsed = NUM_COLORS;
	header.biClrImportant = IMP_COLORS;

	size_t count = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (count == 1) return WRITE_OK;
    return WRITE_ERROR;
}

static enum read_status read_data(FILE* in, struct image img) {
    struct pixel* pixels = img.data;
	uint32_t const padding = define_padding(img.width);
	size_t const width = img.width;
	size_t const height = img.height;
	for (size_t i = 0; i < height; i++) {
		if (fread(pixels + width * i, sizeof(struct pixel), width, in) != width) {
			free(pixels);
			return READ_FILE_ERROR;
		}
		if (fseek(in, padding, SEEK_CUR) != 0) {
			free(pixels);
			return READ_FILE_ERROR;
		}
	}
	return READ_OK;
}


static enum write_status write_data(FILE* out, struct image const img) {
	uint32_t const padding = define_padding(img.width);
	size_t const width = img.width;
	size_t const height = img.height;
	struct pixel* new_pixel = img.data;
	for (size_t i = 0; i < height; i++) {
		if (fwrite(new_pixel, sizeof(struct pixel), width, out) != width)
			return WRITE_ERROR;
		if (fwrite(new_pixel, 1, padding, out) != padding)
			return WRITE_ERROR;
		new_pixel = new_pixel + (size_t)width;
	}
	return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
	if (write_header(out, img) != WRITE_OK)
		return WRITE_ERROR;
	return write_data(out, *img);
}

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header;
	enum read_status header_check = read_header(in, &header);
	if (header_check != READ_OK)
		return header_check;
	img->width = header.biWidth;
	img->height = header.biHeight;
	img->data = malloc(header.biWidth * header.biHeight * sizeof(struct pixel));
	enum read_status read_check = read_data(in, *img);
	//free(header);
	return read_check;
}
