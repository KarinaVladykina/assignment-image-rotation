#include "bmp.h" 
#include "error_mess.h"
#include "file.h" 
#include "image.h" 
#include "rotation.h" 
#include <stdio.h> 
 

 
void write_to_stdout(const char* status) { 
 fprintf(stdout, "%s", status); 
} 
 
void write_to_stderr(const char* status) { 
 fprintf(stderr, "%s", status); 
} 
 
 
 
int main(int argc, char** argv) { 
 (void)argc; (void)argv; 
 if (argc != 3) { 
 fprintf(stderr, "%s", "Некорректный ввод аргументов. Пожалуйста, проверьте ввод"); 
 } 
 else { 
 struct image source = { 0 }; 
 struct image result = { 0 }; 
 enum read_status read_image_status = get_image(argv[1], &source); 
 if (read_image_status >= 2) { 
 write_to_stderr(read_status_out[read_image_status]); 
 return 0; 
 } 
 else write_to_stdout(read_status_out[read_image_status]); 
 
 result = rotate(source); 
 enum write_status write_image_status = write_image(argv[2], &result); 
 if (write_image_status >= 2) { 
 write_to_stderr(write_status_out[write_image_status]); 
 return 0; 
 } 
 else write_to_stdout(write_status_out[write_image_status]); 
 delete_image(result); 
 delete_image(source); 
 
 
 } 
 return 0; 
}
