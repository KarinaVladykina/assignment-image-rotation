#include "rotation.h"

struct image rotate(struct image source) {
	uint64_t x;
	uint64_t y;
	uint64_t amount_of_pixel = source.height * source.width;
	struct image result = create_img(source.height, source.width);
	struct pixel* rotated_pixels = malloc(sizeof (struct pixel) * amount_of_pixel);
	struct pixel* original_pixels = source.data;
	for (size_t i = 0; i < amount_of_pixel; i= i + 1) {
		x = source.height - i / source.width - 1;
    	y = i % source.width;    
		rotated_pixels[y * source.height + x] = original_pixels[i];
	}
	result.data = rotated_pixels;
	return result;
}
